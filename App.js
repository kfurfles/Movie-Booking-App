/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Header from './components/header'
import MovieList from './components/movieList'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
  render() {
    return (
        <View style={[StyleSheet.absoluteFill,styles.container]}>
          <Header/>
          <MovieList/>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding:20
  },
});
