import React , { Component } from 'react'
import { Text, View, Image } from 'react-native'

export default class Movie extends Component{
    constructor(props){
        super(props)
    }
    formatedData(data){
        const dta = data.split('/')
        return new Date(`${dta[1]}/${dta[0]}/${dta[2]}`).toString().substring(4,10);
    }
    listItems = (list, ch = '') => list.join(ch);
    render(){
        return(
            <View style={style.container}>
                <View style={style.imageContainer}>
                    <Image  style={style.image} source={this.props.movie.thumb}></Image>
                </View>
                <View style={style.informations}>
                    <Text style={style.title}>{ this.props.movie.title }</Text>
                    <Text style={style.screening}>Screening on : {  this.formatedData(this.props.movie.dtaScreen) }</Text>
                    <Text style={style.language}>
                        <Text>{ this.listItems(this.props.movie.languages) }</Text> 
                        <Text> { this.listItems(this.props.movie.mode,'/') }</Text>
                    </Text>
                    <Text style={style.language}>{ this.listItems(this.props.movie.genere,', ') }</Text>
                </View>
            </View>
        )
    }
}

const style = {
    title :{
        fontSize:13,
        fontWeight:'600',
        marginBottom:7
    },
    screening:{
        color:'#5a5a5a',
        marginBottom:7
    },
    language:{
        color:'#7a7a7a',
        textTransform:'captalize'
    },
    mode:{},
    genere:{},
    imageContainer: {
        width:100,
        height:150,
        shadowOffset:{  width: -7,  height: 10,  },
        shadowColor: '#000',
        shadowRadius:5,
        shadowOpacity: 0.1,
    },
    image :{
        width: 100, 
        height: 150,
        left:-25,
        position: 'absolute',
        borderRadius: 6,
    },
    container: {
        borderRadius: 6,
        borderColor: '#d6d7da',
        display: 'flex',
        flexDirection:'row',
        position: 'relative',
        marginLeft:25,
        padding:15,
        right:5,
        marginBottom:15,
        backgroundColor: '#fff',
        shadowOffset:{  width: 0,  height: 0,  },
        shadowColor: '#000',
        shadowRadius:7,
        shadowOpacity: 0.1,
    },
    informations: {
        display: 'flex',
        flexDirection:'column',
        position: 'relative',
        left:-10
    }
}