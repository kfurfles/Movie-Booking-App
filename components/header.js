import React , { Component } from "react";
import { Text, View, StyleSheet } from 'react-native'

export default class Header extends Component{
    constructor(props){
        super(props)
        this.state = {}
    }
    render() {
    return (
            <View style={styles.container}>
                <Text style={styles.one}>Special Screening</Text>
                <Text style={styles.legend}>Screening at selected venues</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        marginTop:40,
        marginBottom:10
    },
    one: {
      fontSize:35,
      color:'#333'
    },
    legend: {
        fontSize: 15,
        color: '#cacaca'
    }
});