import React, { Component } from 'react'

import { Text,FlatList, ScrollView } from 'react-native'
import { films } from './../dataFilms.js'
import Movie from './movie.js'

export default class FilmList extends Component{
    constructor(props){
        super(props);
        this.state = {
            listFilms: films
        }
    }
    render(){
        return(
            <ScrollView>
            <FlatList
                data={this.state.listFilms} 
                renderItem={({item, index}) => <Movie className="item" movie={item}/>}            
                keyExtractor={(item, index) => item.title + index}
            />
            </ScrollView>
        )
    }
}