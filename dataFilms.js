export const films = [
    { 
        title: 'Kong Skull Island',
        dtaScreen: '12/01/2018', 
        languages: ['English'], 
        mode: ['2D','3D'], 
        genere: ['Action','Drama'],
        classification: '4',
        thumb: require('/Users/kelvinsilva/Projetos/native_app/assets/kong.png'),
        detailsThumb: 'path thumb',
        cast:[
            { name: 'actor 1', thumb: 'path thumb' },
            { name: 'actor 2', thumb: 'path thumb' },
            { name: 'actor 3', thumb: 'path thumb' }
        ],
        synopsis: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi est quos nemo cupiditate minus similique ullam mollitia a optio quibusdam nihil provident maxime consectetur necessitatibus, velit quo error ducimus aliquid.'
    },
    { 
        title: 'Rogue One - A Star Wars Story',
        dtaScreen: '18/01/2018', 
        languages: ['English'], 
        mode: ['3D'], 
        genere: ['Drama','Adventure'],
        classification: '5',
        thumb: require('/Users/kelvinsilva/Projetos/native_app/assets/rogue.png'),
        detailsThumb: 'path thumb',
        cast:[
            { name: 'actor 1', thumb: 'path thumb' },
            { name: 'actor 2', thumb: 'path thumb' },
            { name: 'actor 3', thumb: 'path thumb' }
        ],
        synopsis: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi est quos nemo cupiditate minus similique ullam mollitia a optio quibusdam nihil provident maxime consectetur necessitatibus, velit quo error ducimus aliquid.'
    },
    { 
        title: 'Wonder Woman',
        dtaScreen: '22/02/2018', 
        languages: ['english'], 
        mode: ['3D'], 
        genere: ['Action','Adventure'],
        classification: '3',
        thumb: require('/Users/kelvinsilva/Projetos/native_app/assets/wonder.png'),
        detailsThumb: 'path thumb',
        cast:[
            { name: 'actor 1', thumb: 'path thumb' },
            { name: 'actor 2', thumb: 'path thumb' },
            { name: 'actor 3', thumb: 'path thumb' }
        ],
        synopsis: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi est quos nemo cupiditate minus similique ullam mollitia a optio quibusdam nihil provident maxime consectetur necessitatibus, velit quo error ducimus aliquid.'
    }
]