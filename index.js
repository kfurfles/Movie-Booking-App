import { AppRegistry } from 'react-native';
import native_app from './App';

AppRegistry.registerComponent('native_app', () => native_app);
